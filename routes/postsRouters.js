const { Router } = require('express');
const { getAllPosts, getPostById, createNewPost, updatePost, deletePost } = require('../controllers/PostsControllers');
const router = Router();

router.get('/', getAllPosts);
router.get('/:shortId', getPostById);
router.post('/', createNewPost);
router.patch('/', updatePost);
router.delete('/', deletePost);

module.exports = router;