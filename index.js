const express = require('express');
const mongoose = require('mongoose');

const app = express();
const PORT = 8080;
const postsRouter = require('./routes/postsRouters');

mongoose.connect('')
app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello, World!');
})

app.use('/posts', postsRouter);
// error handling

app.listen(PORT, () => {
    console.log('The Server is listening at PORT 8080');
})