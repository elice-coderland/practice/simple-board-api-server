const { Post } = require('../models');
const {asyncHandler} = require('../utils/asyncHandler');

// 게시글 목록 가져오기
exports.getAllPosts = asyncHandler(async (req, res) => {
    const allPosts = await Post.find({});
    if (allPosts.length < 1) {
        throw new Error('There is no post.');
    }
    res.send(allPosts);
});

// 게시글 상세 가져오기
exports.getPostById = asyncHandler(async (req, res) => {
    const { shortId } = req.params;
    const postById = await Post.findOne({ shortId });
    if (!postById) {
        throw new Error('There is no such post with the ID.');
    }
    res.send(postById);
});

// 게시글 작성하기
exports.createNewPost = asyncHandler(async (req, res) => {
    const { title, content } = req.body;
    if (!title || !content) {
        throw new Error('Please insert title or content.');
    }
    const newPost = await Post.create({ title, content });
    res.send(newPost);
});

// 게시글 수정하기
exports.updatePost = asyncHandler(async (req, res) => {
    const { shortId } = req.params;
    const { title, content } = req.body;

    if (!title || !content) {
        throw new Error('Please insert title or content');
    }
    const updateOne = await Post.findOneAndUpdate({ shortId }, { title, content });
    res.send(updateOne);
});

// 게시글 삭제 하기
exports.deletePost = asyncHandler(async (req, res) => {
    const { shortId } = req.params;
    if (!shortId) throw new Error('There is no such post with the ID.');

    await Post.findOneAndDelete({ shortId });
    res.status(204).send();
})