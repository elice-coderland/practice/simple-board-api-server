const { Schema } = require('mongoose');
const {shortId} = require('./type/short-id');

const PostSchema = new Schema({
    shortId,
    title: String,
    content: String,
    author: String,
}, {
    timestamps: true,
});

module.exports = PostSchema;